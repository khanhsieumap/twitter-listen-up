const Twit = require('twit')
const _ = require('lodash')
const ltr = require('lucene-to-regex')

const T = new Twit({
  consumer_key: process.env.TWIT_CONSUMER_KEY || '',
  consumer_secret: process.env.TWIT_CONSUMER_SECRET || '',
  access_token: process.env.TWIT_ACCESS_TOKEN || '',
  access_token_secret: process.env.TWIT_ACCESS_TOKEN_SECRET || '',
  timeout_ms: 60 * 1000,
})

let _bp = null
let _stream = null
const _terms = {}
const _map = {}

const addQuery = (id, query) => {
  _addQuery(id, query)
  startStream()
}

const _addQuery = (id, query) => {
  const terms = _splitQuery(query)
  const regex = ltr.toRegex(query, 'i')
  _removeQuery(id)
  _.each(terms, term => {
    if (!_terms[term]) {
      _terms[term] = 0
    }
    _terms[term]++
  })

  _map[id] = {
    query,
    terms,
    regex,
    on: true
  }
}

const removeQuery = id => {
  _removeQuery()
  startStream()
}

const _removeQuery = id => {
  if (!_map[id])
    return

  _.each(_map[id].terms, term => {
    _terms[term] && _terms[term]--
    if (_terms[term] === 0)
      delete _terms[term]
  })

  delete _map[id]
}

const startStream = (options) => {
  stopStream()
  _bp.logger.info('Twitter is now streaming...')
  _bp.logger.info(`Track: ${JSON.stringify(_.keys(_terms))}`)
  _stream = T.stream('statuses/filter', Object.assign({ track: _.keys(_terms), tweet_mode: 'extended' }, options))
  _stream.on('tweet', _handleTweetStream)
}

const stopStream = () => _stream && _stream.stop() && _stream.removeAllListeners('tweet') && _bp.logger.info('Twitter has stopped streaming')

const _handleTweetStream = tweet => {
  if (tweet.retweeted_status)
    return

  _bp.logger.debug(`Got tweet: ${tweet.id_str}`)

  const text = tweet.truncated
    ? tweet.extended_tweet.full_text || tweet.text
    : tweet.full_text || tweet.text

  _bp.logger.debug(`Text: ${text}`)
  _.each(_map, (data, id) => {
    if (!data.on || !data.regex.test(text))
      return
    _bp.messenger.sendTemplate(id, tweetTemplate([tweet]))
  })
}

const _splitQuery = query => {
  return query.split(/\s?(?:AND|OR|\|\||NOT|AND\sNOT|OR\sNOT|\(\)|\&\&)\s?/)
}

const togglePush = (id, on) => {
  if (!_map[id])
    return

  _map[id].on = on
}

const tweetTemplate = (tweets) => {
  const elements = tweets.map(tweet => {
    const image_url = _.get(tweet, 'entities.media[0].media_url_https')
      || _.get(tweet, 'retweeted_status.entities.media[0].media_url_https')
      || _.get(tweet, "user.profile_image_url_https").replace(/_normal\./, '.')
      || ''

    return {
      title: `${tweet.user.name}: on Twitter`,
      image_url,
      subtitle: tweet.full_text || tweet.text,
      default_action: {
        type: 'web_url',
        // messenger_extensions: true,
        url: `https://twitter.com/statuses/${tweet.id_str}`,
        webview_height_ratio: "full",
      },
      buttons: [{
        type: "element_share"
      }]
    }
  });

  return {
    template_type: 'generic',
    elements,
  }
}

module.exports = {
  setBp: bp => _bp = bp,
  search: (query, options) => T.get('search/tweets', Object.assign({ q: query, tweet_mode: 'extended' }, options)),
  addQuery,
  removeQuery,
  togglePush,
  tweetTemplate
};

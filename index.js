const Promise = require('bluebird')
const _ = require('lodash')
const TwitterListenUp = require('./tweet')

module.exports = function (bp) {

  TwitterListenUp.setBp(bp)

  bp.rivescript.setUtf8(true)
  db = bp.db.kvs

  const defaultOptions = { typing: 1000 }

  const human = {
    init: /^(GET_STARTED|hello|hi|test|hey|holla)/i,
    stop: /^(exit|bye|goodbye|quit|done|leave|stop)/i,
    query: /^(query|(?:change|listen)(?:\sto)?)\s(.+)/i,
  }
  bp.yesno = {
    quick_replies: [{
      title: 'Cancel',
      payload: 'CANCEL'
    }, {
      title: 'No. Let me change it',
      payload: 'NO'
    }, {
      title: 'Yes',
      payload: 'YES'
    }]
  }
  const bot = {
    listen: () => _.sample(['I am listening...', 'Feed me some query!', 'Tell me']),
    check: (query) => _.sample([`Your query is ${query}. Do you want to change it?`]),
    accept: () => _.sample(['Ok', 'Good', 'Thanks']),
    try: (hasTweet) => hasTweet ? 'Something like this?' : `There wasn't any tweet with given query. Still wanna save?`,
    save: () => _.sample(['Saved. Ping me anytime.']),
    bye: () => _.sample(['Ok bye! Til\' next time.', 'Ok doei!', 'Doei!!!', 'Ciaooo']),
    fail: () => 'AAAA((@@@@JJJ - Sorry, server error, please try again.',
    pause: () => 'Ok ok. Stop pushing new tweets. To turn it on again, try \'resume\'',
    resume: () => 'Boooooom'
  }

  bp.checkQuery = (rawInput, event, send) => {
    const sendText = (txt, options) => bp.messenger.sendText(event.user.id, txt, options = defaultOptions)
    const input = human.query.exec(rawInput)
    const query = input ? input[2] : rawInput
    bp.logger.info(`Got input query: ${query}`)

    bp.stopPushing(event)

    TwitterListenUp.search(query, { count: 5 })
      .then(res => sendText(bot.accept()).then(() => res.data.statuses))
      .then(tweets => tweets.length && bp.messenger.sendTemplate(event.user.id, TwitterListenUp.tweetTemplate(tweets)).then(() => tweets))
      .then(tweets => send(bot.try(!!tweets), bp.yesno))
      .then(() => db.set(`users/id/${event.user.id}`, query, 'tempQuery'))
      .catch(err => errHandler(err, send))
  }

  bp.saveQuery = (event, send) => {
    db.get(`users/id/${event.user.id}`, 'tempQuery')
      .then(tempQuery => db.set(`users/id/${event.user.id}`, tempQuery, 'query').then(() => tempQuery))
      .then(query => TwitterListenUp.addQuery(event.user.id, query))
      .then(send(bot.save(), defaultOptions))
      .catch(err => errHandler(err, send))
  }

  // Resume pushing
  bp.startPushing = (event, send) => {
    db.set(`users/id/${event.user.id}`, true, 'on')
      .then(() => TwitterListenUp.togglePush(event.user.id, true))
      .then(() => send && send(bot.pause(), defaultOptions))
  }

  // Pause pushing
  bp.stopPushing = (event, send) => {
    db.set(`users/id/${event.user.id}`, false, 'on')
      .then(() => TwitterListenUp.togglePush(event.user.id, false))
      .then(() => send && send(bot.pause(), defaultOptions))
  }

  const errHandler = (err, send) => {
    bp.logger.error(err)
    send && send(bot.fail())
  }
}
